<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Log\Writer;
use Monolog\Logger as Monolog;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Helper as Helper;
use App\User;
use Config;
use View;
use Redirect; 
use Validator;
use Response;
use Auth;
use Crypt;
use Cookie;
use Hash;
use Lang;
use JWTAuth;
use Input;


class APIController extends Controller
{
	
   /* @method : register
	* @param : email,password,deviceID,firstName,lastName
	* Response : json
	* Return : token and user details
	* Author : kundan Roy
	* Calling Method : get	
    */

    public function register(Request $request,User $user)
    {        
    	$input['first_name'] 	= $request->get('firstName');
    	$input['last_name'] 	= $request->get('lastName'); 
    	$input['email'] 		= $request->get('email'); 
    	$input['password'] 	= Hash::make($request->get('password'));
    	$input['deviceID'] 	= ($request->get('deviceID'))?$request->get('deviceID'):'';

        //Server side valiation
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:t_user',
            'firstName' => 'required',
            'lastName' => 'required',
            'password' => 'required',
        ]);
        // Return Error Message
        if ($validator->fails()) {

        	$error_msg	=	[];
	        foreach ( $validator->messages()->all() as $key => $value) {
	        			array_push($error_msg, $value);		
	        		}
	        		 		
          	return Response::json(array(
	          	'status' => 0,
	            'message' => $error_msg[0],
	            'data'	=>	''
	            )
          	);
        }

    	$user = User::create($input); 
        $helper = new Helper;
        $helper->createCompanyGroup($request->get('email'),$user->userID);
        return response()->json([ "status"=>1,"message"=>"success",'data'=>$request->except('password')]);
    }
   /* @method : login
	* @param : email,password and deviceID
	* Response : json
	* Return : token and user details
	* Author : kundan Roy	
    */
    public function login(Request $request)
    {
    	$input = $request->all();
    	if (!$token = JWTAuth::attempt(['email'=>$request->get('email'),'password'=>$request->get('password')])) {
            return response()->json(['result' => 'wrong email or password.']);
        }

        $user = JWTAuth::toUser($token); 
        $data['deviceToken'] = $token;
        $data['userID'] = $user->userID;
        $data['firstName'] = $user->first_name;
        $data['lastName'] = $user->last_name;
        $data['email'] = $user->email;

        $user = User::find($user->userID);
        $user->deviceID  = $request->get('deviceID');
        $user->save();

    	return response()->json([ "status"=>1,"message"=>"success" ,'data' => $data ]);

    }
   /* @method : get user details
	* @param : Token and deviceID
	* Response : json
	* Return : User details	
   */
   
    public function get_user_details(Request $request)
    {
    	 
    	$user = JWTAuth::toUser($request->get('token'));
        $data['userID'] = $user->userID;
        $data['firstName'] = $user->first_name;
        $data['lastName'] = $user->last_name;
        $data['email'] = $user->email;
        return response()->json([ "status"=>1,"message"=>"success" ,'data' => $data ]);
    }
    
}
